# README for xdg-themes-stackrpms
This is a collection of my xdg themes. It presently only contains `bgstack15-red`.

# Contents and history
* bgstack15-red (formerly bgstack15-red-k25)
  * This theme is a combination of whatever the Korora Linux 22-25 Cinnamon theme was, and the customized Arc theme for XFCE. It depends on Numix circle icon theme. Korora Linux was my first Linux distro for desktop use (November 2015, Korora 22), and I switched off of it around Korora 25 (Fedora 25) which was one of the last releases before Chris Smart and Ian Firns dropped it. My main distros were then Fedora 25 (Cinnamon and XFCE) through around Fedora 28 when I started switching to Devuan GNU+Linux. In 2019 I switched to Fluxbox so this package is itself a little unnecessary.

# Alternatives
See [fluxbox-themes-stackrpms](https://gitlab.com/bgstack15/fluxbox-themes-stackrpms) for the Fluxbox themes I collect.

# How to use
* Build as a [dpkg](https://gitlab.com/bgstack15/stackrpms/-/tree/master/xdg-themes-stackrpms)
* Install from the [Open Build Service](https://build.opensuse.org/package/show/home:bgstack15/xdg-themes-stackrpms)

# Changelog
2022-10-05 fix #3: Fix gtk2 gtkrc automnemonics
